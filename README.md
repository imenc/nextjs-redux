# DRF React
Next.js - Build Full Stack Apps with Next.js using Redux

## Udemy Course Page
[https://www.udemy.com/course/nextjs-build-full-stack-apps-with-nextjs-using-redux](https://www.udemy.com/course/nextjs-build-full-stack-apps-with-nextjs-using-redux)

## Course Git
[https://github.com/ghulamabbas2/bookit](https://github.com/ghulamabbas2/bookit)

## Project Folder
```bash
~/udemy/next-redux/bookit
```